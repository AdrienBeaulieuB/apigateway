import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { UsersModule } from './users/users.module';
import { AuthModule } from './auth/auth.module';
import { MetricsModule } from './metrics/metrics.module';
import { HttpModule } from '@nestjs/axios';

@Module({
  imports: [UsersModule, AuthModule, MetricsModule, HttpModule],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
