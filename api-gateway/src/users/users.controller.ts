import { Body, Controller, Get, Param } from '@nestjs/common';
import { UsersService } from './users.service';

@Controller('users')
export class UsersController {
  constructor(private readonly userService: UsersService) {}

  @Get()
  async getusers() {
    const users = await this.userService.findAll();
    return users.data;
  }

  @Get(':id')
  async getUser(@Param('id') id: string) {
    const user = await this.userService.findOne(id);
    console.log(user);
    return user.data;
  }
}
