import { Injectable } from '@nestjs/common';
import { HttpService } from '@nestjs/axios';
import { AxiosResponse } from 'axios';
import {Observable} from "rxjs";

@Injectable()
export class UsersService {
  constructor(private readonly httpService: HttpService) {}

  findAll(): Promise<AxiosResponse<any[]>> {
    return this.httpService.axiosRef.get('http://localhost:3001/users');
  }

  findOne(id: string): Promise<AxiosResponse<any>> {
    return this.httpService.axiosRef.get(`http://localhost:3001/users/${id}`);
  }
}
